fbrawl = {}
fbrawl.T = minetest.get_translator("fantasy_brawl")


dofile(minetest.get_modpath("fantasy_brawl") .. "/src/SETTINGS.lua")


arena_lib.register_minigame("fantasy_brawl", {
  prefix = fbrawl_settings.prefix,
  icon = "fbrawl_icon.png",
  temp_properties = {
    classes = {},  -- pl_name: string = class: {}
    match_started = false,
    scores = {}  -- pl_name: string = score: number
  },
  player_properties = {
    kills = 0,
    deaths = 0,
    ultimate_recharge = 0,
    hit_by = {}, -- {"player1" = <damage>, ...}
    is_invulnerable = false
  },
  disabled_damage_types = {"fall"},
  hotbar = {slots = 4, background_image = "fbrawl_hotbar.png"},
  load_time = fbrawl_settings.loading_time,
  show_nametags = false,
  show_minimap = false,
  celebration_time = fbrawl_settings.celebration_time,
  time_mode = "decremental",
  join_while_in_progress = true
})


dofile(minetest.get_modpath("fantasy_brawl") .. "/src/deps/visible_wielditem.lua")

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/hud.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/sounds.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/score.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/invulnerability.lua")

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/utils.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/blood_effect.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/health_bar.lua")

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/respawn/respawn.lua")

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/classes_system.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/skill_templates/meteors.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/class_selector_formspec.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/hp_regen.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/skill_status_hud.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/book_pedestal.lua")

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/arena_lib/callbacks.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/arena_lib/utils.lua")

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/warrior/warrior.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/mage/mage.lua")

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/commands.lua")